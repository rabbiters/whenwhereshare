package org.cpe22.wws;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Act_Create_Share extends Activity {
	private Button goShareAll;
	private Event event;
	private TextView title;
	private TextView date;
	private TextView desc;
	private TextView id;
	private double latitude,longtitude;
	private ProgressDialog progress;
	private void getIntentFromWhere() {
		event = getIntent().getParcelableExtra("event");
		latitude = getIntent().getDoubleExtra("lat",0);
		longtitude = getIntent().getDoubleExtra("long",0);
	}
	private void initialBtn(){
		goShareAll = (Button) findViewById(R.id.btn_ShareAll);
		progress = new ProgressDialog(this);
		goShareAll.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				(new AsyncTask<Void, Void, Void>(){
					@Override
					protected void onPreExecute() {
						super.onPreExecute();
						startProgressDialog();
					}
					protected Void doInBackground(Void... params) {
						HttpClient httpclient = new DefaultHttpClient();
						HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/add");
						try {
							/*Add your data*/
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(9);
							nameValuePairs.add(new BasicNameValuePair("eventID", event.getEventID()));
							nameValuePairs.add(new BasicNameValuePair("title", event.getTitle()));
							nameValuePairs.add(new BasicNameValuePair("creatorUID", event.getCreatorID()));
							nameValuePairs.add(new BasicNameValuePair("name", event.getName()));
							nameValuePairs.add(new BasicNameValuePair("date", event.getDate()));
							nameValuePairs.add(new BasicNameValuePair("time", event.getTime()));
							nameValuePairs.add(new BasicNameValuePair("description", event.getDesc()));
							nameValuePairs.add(new BasicNameValuePair("lat", String.valueOf(latitude)));
							nameValuePairs.add(new BasicNameValuePair("lon", String.valueOf(longtitude)));
//							UrlEncodedFormEntity form = new UrlEncodedFormEntity(nameValuePairs);
//							form.setContentEncoding(HTTP.UTF_8);
							httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
//							httppost.setEntity(form);

							/*Execute HTTP Post Request*/
//							HttpResponse response = httpclient.execute(httppost);
							httpclient.execute(httppost);

						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						}
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						super.onPostExecute(result);
						
//						Intent intent = new Intent(Intent.ACTION_SEND);
//						intent.setType("text/plain");
//						intent.putExtra(Intent.EXTRA_TEXT, "http://whenwhereshare.appspot.com/event?eventID="+event.getEventID());
//						startActivity(Intent.createChooser(intent, "Share with"));
						
						stopProgressDialog();
						setResult(RESULT_OK, new Intent().putExtra("link", "http://whenwhereshare.appspot.com/event?eventID="+event.getEventID()));
						finish();
					}
				}).execute();
			}
		});
	}
	private void initial(){
		event = getIntent().getParcelableExtra("event");
		title = (TextView) findViewById(R.id.txt_eventTitle);
		title.setText(event.getTitle());
		date = (TextView) findViewById(R.id.txt_eventDate);
		date.setText(event.getDate() + " " + event.getTime());
		desc = (TextView) findViewById(R.id.txt_eventDesc);
		desc.setText(event.getDesc());	
		id = (TextView) findViewById(R.id.txt_eventIDe);
		id.setText(event.getEventID());
	}
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        /*-----------Full Screen ------------*/
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        /*-----------------------------------*/
	        setContentView(R.layout.create_share);
	        initial();
	        getIntentFromWhere();
	        initialBtn();						
	 }
	private boolean dialogIsWorking = false;
	private void startProgressDialog(){
		progress.setCancelable(false);
		progress.setMessage("sharing");
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}
}
