package org.cpe22.wws;

public class Place {
	public String name;
	public double lat, lon;
	public String address;
	public String city;
	public String postalCode;
}
