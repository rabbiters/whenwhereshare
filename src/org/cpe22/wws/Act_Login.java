package org.cpe22.wws;

import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class Act_Login extends Activity {

	private Facebook facebook;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        /*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
		setContentView(R.layout.login);
		
		Button btnLogin = (Button)findViewById(R.id.btnLogin);
		btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				AuthenFacebook();
			}
		});
		Button btnExit = (Button)findViewById(R.id.btnExit);
		btnExit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		facebook = new Facebook("249069241853063");
//		AuthenFacebook();
	}
	
	/*authen facebook*/
	private void AuthenFacebook(){
    	facebook.authorize(this, new String[]{"offline_access", "publish_stream"}, -1, new DialogListener() {
            @Override
            public void onComplete(Bundle values) {
            	Act_Main.token = facebook.getAccessToken();
            	Act_Main.editor.putString("token", Act_Main.token);
            	
				try {
					JSONObject json = Util.parseJson(facebook.request("me"));
					Act_Main.uid = json.getString("id");
					Act_Main.editor.putString("uid", Act_Main.uid);
					
					Act_Main.name = json.getString("name");
					Act_Main.editor.putString("name", Act_Main.name);
					
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (FacebookError e) {
					e.printStackTrace();
				}
				
				Act_Main.editor.commit();
				startActivity(new Intent(Act_Login.this, Act_Main.class));
				finish();
            }

            @Override
            public void onFacebookError(FacebookError error) {
            	Toast.makeText(getBaseContext(), "Facebook Error", 0).show();
            }

            @Override
            public void onError(DialogError e) {
            	Toast.makeText(getBaseContext(), "Internet connection error", 0).show();
            }

            @Override
            public void onCancel() {
            	Toast.makeText(getBaseContext(), "Cancel Facebook", 0).show();
            }
        });
    }

}
