package org.cpe22.wws;

import android.os.Parcel;
import android.os.Parcelable;

public class Event implements Parcelable {
	private String eventID;
	private String title;
	private String creatorUID;
	private String name;
	private String date;
	private String time;
	private String description;

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		// TODO Auto-generated method stub
		out.writeString(eventID);
		out.writeString(title);
		out.writeString(creatorUID);
		out.writeString(name);
		out.writeString(date);
		out.writeString(time);
		out.writeString(description);
	}

	public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
		public Event createFromParcel(Parcel in) {
			return new Event(in);
		}

		public Event[] newArray(int size) {
			return new Event[size];
		}
	};

	private Event(Parcel in) {
		eventID = in.readString();
		title = in.readString();
		creatorUID = in.readString();
		name = in.readString();
		date = in.readString();
		time = in.readString();
		description = in.readString();
	}

	public Event() {
		// TODO Auto-generated constructor stub
		eventID = Long.toString((System.currentTimeMillis()%1000000), 16);
		creatorUID = Act_Main.uid;
		name = Act_Main.name;
	}

	/************************ GET SET methods **********************/
	public String getEventID() {
		return this.eventID;
	}

	public String getTitle() {
		return this.title;
	}

	public String getCreatorID() {
		return this.creatorUID;
	}
	
	public String getName() {
		return this.name;
	}

	public String getDate() {
		return this.date;
	}

	public String getTime() {
		return this.time;
	}

	public String getDesc() {
		return this.description;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCreator(String uid) {
		this.creatorUID = uid;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setDesc(String desc) {
		this.description = desc;
	}
	/**************************************************************/
}
