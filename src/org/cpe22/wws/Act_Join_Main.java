package org.cpe22.wws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.qrcode.*;

public class Act_Join_Main extends Activity  {
	private EditText eventID;
	private Button joinBtn;
	private Button QRscanner;
	private String event_id;
	private String uID;
	private void initial(){
		eventID = (EditText) findViewById(R.id.edt_eventID);
		QRscanner = (Button) findViewById(R.id.btn_scanner);
		joinBtn = (Button) findViewById(R.id.btn_joinMain);
		progress = new ProgressDialog(this);
		uID = Act_Main.uid;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	 /*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
    	setContentView(R.layout.join_main);
    	initial();
    	joinBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	event_id = eventID.getText().toString().toLowerCase();
            	if(event_id.length() == 0)
            		return;
            	Toast.makeText(getApplicationContext(), "Joiner ID:"+uID +"EVENT ID:"+event_id, Toast.LENGTH_SHORT).show();
            	
            	(new AsyncTask<Void, Void, String>(){
    				@Override
    				protected void onPreExecute() {
    					super.onPreExecute();
    					startProgressDialog();
    				}
    				@Override
    				protected String doInBackground(Void... params) {
    					HttpClient httpclient = new DefaultHttpClient();
    					HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/event");
    					HttpResponse response;
    					InputStream stream = null;
    					
    					try {
    						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
							nameValuePairs.add(new BasicNameValuePair("eventID", event_id));
							httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
							
    						response = httpclient.execute(httppost);
    						
    						stream = response.getEntity().getContent();
    					} catch (ClientProtocolException e) {
    						e.printStackTrace();
    					} catch (IOException e) {
    						e.printStackTrace();
    					}
    					
    			        BufferedReader reader = null;
						try {
							reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
						}
    			        String jsonString = "";
    			        String line;
    			        try {
    						while ((line = reader.readLine()) != null) {
    							jsonString += line;
    						}
    					} catch (IOException e) {
    						e.printStackTrace();
    					}
    			        
    					return jsonString;
    				}
    				@Override
    				protected void onPostExecute(String result) {
    					super.onPostExecute(result);
    					stopProgressDialog();
    					if(result.compareTo("EMPTY") == 0){
    						Toast.makeText(getBaseContext(), "Event not found", 1).show();
    					} else {
//    						Toast.makeText(getBaseContext(), result, 1).show();
    						startActivityForResult(new Intent(Act_Join_Main.this, Act_Join_ShowDetail.class).putExtra("eventID", event_id).putExtra("eventJson", result), 1);
    					}
    				}
            	}).execute();
            	
            }
    	});
    	
    	QRscanner.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	 IntentIntegrator.initiateScan(Act_Join_Main.this);
            }
    	});
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (IntentIntegrator.REQUEST_CODE == requestCode) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				eventID.setText(getEventID(contents));
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, "Error reading barcode", Toast.LENGTH_SHORT).show();
			}
		} else {
			if(resultCode == RESULT_OK){
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
    
    private String getEventID(String contents){
    	String extracted= "";
    	if(contents.length() > 0){
    		extracted = contents.substring(contents.indexOf("=")+1,contents.length());
    	}
    	return extracted;
    }
    
    private ProgressDialog progress;
	private boolean dialogIsWorking = false;
	private void startProgressDialog(){
		progress.setCancelable(false);
		progress.setMessage("loading");
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}
	
}
