package org.cpe22.wws;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class Act_Join_ShowDetail extends Activity {
	private TextView eventID;
	private TextView creator;
	private TextView title;
	private TextView description;
	private TextView time;
	private ListView listOfJoiner;
	private String jsonString = null;
	private JSONObject json = null;
	private Button btn_join = null;
	private String joinerID[] = null;
	private String joiner[] = null;
	protected void initial() {
		eventID = (TextView) findViewById(R.id.txtShow_eventID);
		creator = (TextView) findViewById(R.id.txtShow_eventCreator);
		title = (TextView) findViewById(R.id.txtShow_eventTitle);
		description = (TextView) findViewById(R.id.txtShow_Desc);
		time = (TextView) findViewById(R.id.txtShow_eventTime);
		listOfJoiner = (ListView) findViewById(R.id.listOfJoiner);
		btn_join = (Button)findViewById(R.id.btn_JoinShare);
		
		jsonString = getIntent().getStringExtra("eventJson");

		try {
			json = Util.parseJson(jsonString);
			eventID.setText(getIntent().getStringExtra("eventID"));
			creator.setText(json.getString("creator"));
			title.setText(json.getString("title"));
			description.setText(json.getString("description"));
			time.setText(json.getString("date") + " " + json.getString("time"));
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(getBaseContext(), jsonString, 1).show();
		} catch (FacebookError e) {
			e.printStackTrace();
		}
		
		/*joiner list*/
		try {
			int numJoiner = json.getJSONArray("joiner").length();
			joinerID = new String[numJoiner];
			joiner = new String[numJoiner];
			for(int i=0;i<numJoiner;i++){
				joinerID[i] = json.getJSONArray("joiner").getJSONObject(i).getString("uid");
				joiner[i] = json.getJSONArray("joiner").getJSONObject(i).getString("name");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		listOfJoiner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, joiner));
		listOfJoiner.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.facebook.com/"+joinerID[arg2])));
			}
		});
		
		/*join button*/
		btn_join.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivityForResult(new Intent(Act_Join_ShowDetail.this, Act_Create_Where.class).putExtra("eventID", getIntent().getStringExtra("eventID")), 1);
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*-----------Full Screen ------------*/
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/*-----------------------------------*/
		setContentView(R.layout.join_show);
		initial();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 1 && resultCode == RESULT_OK){
			setResult(RESULT_OK, new Intent().putExtra("eventID", getIntent().getExtras().getString("eventID")));
			finish();
		}
	}
	
}
