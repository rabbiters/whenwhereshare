package org.cpe22.wws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class Act_Create_Where extends MapActivity {
	public static final String TAG = "GoogleMapsActivity";
	private MapView mapView;
	private LocationManager locationManager;
//	private Geocoder geocoder;
	private Location location;
	private LocationListener locationListener;
	private CountDownTimer locationtimer;
	private MapController mapController;
	private MapOverlay mapOverlay = new MapOverlay();
	private Event event = null;
	private Button goShare;
	private String eventID = null;

	private void getIntentFromWhen() {
		event = getIntent().getParcelableExtra("event");
		
		/*---------- JOIN ----------*/
		if(event == null){
			eventID = getIntent().getStringExtra("eventID");
			goShare.setOnClickListener(new View.OnClickListener() {
	            public void onClick(View v) {
					locationManager.removeUpdates(locationListener);
					(new AsyncTask<Void, Void, String>(){
						@Override
						protected void onPreExecute() {
							super.onPreExecute();
							startProgressDialog();
						}
						@Override
						protected String doInBackground(Void... params) {
							HttpClient httpclient = new DefaultHttpClient();
							HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/join");
							InputStream stream = null;
							
							try {
								/*Add your data*/
								List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
								nameValuePairs.add(new BasicNameValuePair("eventID", eventID));
								nameValuePairs.add(new BasicNameValuePair("uid", Act_Main.uid));
								nameValuePairs.add(new BasicNameValuePair("name", Act_Main.name));
								nameValuePairs.add(new BasicNameValuePair("lat", String.valueOf(location.getLatitude())));
								nameValuePairs.add(new BasicNameValuePair("lon", String.valueOf(location.getLongitude())));
								httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

								/*Execute HTTP Post Request*/
								HttpResponse response = httpclient.execute(httppost);
								
								stream = response.getEntity().getContent();

							} catch (ClientProtocolException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
							
							BufferedReader reader = null;
							try {
								reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
							} catch (UnsupportedEncodingException e1) {
								e1.printStackTrace();
							}
					        String jsonString = "";
					        String line;
					        try {
								while ((line = reader.readLine()) != null) {
									jsonString += line;
								}
							} catch (IOException e) {
								e.printStackTrace();
							}
					        
							return jsonString;
						}
						@Override
						protected void onPostExecute(String result) {
							super.onPostExecute(result);
							stopProgressDialog();
							if(result.compareTo("SUCCESS") != 0)
								Toast.makeText(getBaseContext(), "The event has already started", 1).show();
							setResult(RESULT_OK);
							finish();
						}
					}).execute();
	        	}
	        });
		}
//		Toast.makeText(getApplicationContext(), "UID"+event.getCreatorID(), Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
        /*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
		setContentView(R.layout.create_where);
		initComponents();
		getIntentFromWhen();
		//mapView.setBuiltInZoomControls(true);
		mapView.setSatellite(true);
		mapController = mapView.getController();
		mapController.setZoom(16);
		
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		checkEnableGPS();
		
		if (locationManager == null) {
			Toast.makeText(getApplicationContext(), "Location Manager Not Available", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
		location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (location == null)
			location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		
		if (location != null) {
			double lat = location.getLatitude();
			double lng = location.getLongitude();
//			Toast.makeText(getApplicationContext(), "Location Are" + lat + ":" + lng, Toast.LENGTH_SHORT).show();

			GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
			mapController.animateTo(point, new Message());
			mapOverlay.setPointToDraw(point);
			List<Overlay> listOfOverlays = mapView.getOverlays();
			listOfOverlays.clear();
			listOfOverlays.add(mapOverlay);
		}
		
		locationListener = new LocationListener() {
			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			}

			@Override
			public void onProviderEnabled(String arg0) {
			}

			@Override
			public void onProviderDisabled(String arg0) {
				checkEnableGPS();
			}

			@Override
			public void onLocationChanged(Location l) {
				location = l;
//				locationManager.removeUpdates(this);
				if (l.getLatitude() == 0 || l.getLongitude() == 0) {
				} else {
					double lat = l.getLatitude();
					double lng = l.getLongitude();
					Toast.makeText(getApplicationContext(), "Location Are" + lat + ":" + lng, Toast.LENGTH_SHORT).show();
					
					GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));
					mapController.animateTo(point, new Message());
					mapOverlay.setPointToDraw(point);
					List<Overlay> listOfOverlays = mapView.getOverlays();
					listOfOverlays.clear();
					listOfOverlays.add(mapOverlay);
				}
			}
		};
		
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		else
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		
		locationtimer = new CountDownTimer(30000, 5000) {
			@Override
			public void onTick(long millisUntilFinished) { }

			@Override
			public void onFinish() {
				locationManager.removeUpdates(locationListener);
			}
		};
		locationtimer.start();
	}

	public MapView getMapView() {
		return this.mapView;
	}

	private void initComponents() {
		progress = new ProgressDialog(this);
		mapView = (MapView) findViewById(R.id.view_map);
		goShare = (Button) findViewById(R.id.btn_Share);
		goShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
				locationManager.removeUpdates(locationListener);
            	Intent intent = new Intent(getApplicationContext(),Act_Create_Share.class);
            	intent.putExtra("event", event);
            	intent.putExtra("lat", location.getLatitude());
            	intent.putExtra("long", location.getLongitude());
            	startActivityForResult(intent, 1);
        	}
        });
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 1 && resultCode == RESULT_OK){
			setResult(RESULT_OK, data);
			finish();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	class MapOverlay extends Overlay {
		private GeoPoint pointToDraw;

		public void setPointToDraw(GeoPoint point) {
			pointToDraw = point;
		}

		public GeoPoint getPointToDraw() {
			return pointToDraw;
		}

		@Override
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) {
			super.draw(canvas, mapView, shadow);

			Point screenPts = new Point();
			mapView.getProjection().toPixels(pointToDraw, screenPts);

			Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pingreen);
			canvas.drawBitmap(bmp, screenPts.x, screenPts.y - 24, null);
			return true;
		}
	}

	private void checkEnableGPS() {
		/* Go to GPS setting if it is not set */
		if (!locationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER) && !locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
			Toast.makeText(getBaseContext(), "Please enable GPS", 1).show();
			startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		checkEnableGPS();
	}
	
	private ProgressDialog progress;
	private boolean dialogIsWorking = false;
	private void startProgressDialog(){
		progress.setCancelable(false);
		progress.setMessage("joining");
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}
}
