package org.cpe22.wws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class Act_StartEvent extends Activity {
	private TextView eventID;
	private TextView creator;
	private TextView title;
	private TextView description;
	private TextView time;
	private ListView listOfJoiner;
	private Button btn_startEvent = null;
	
//	private JSONObject json = null;
	private String joinerID[] = null;
	private String joiner[] = null;
	
	private boolean share = false;
	protected void initial() {
		progress = new ProgressDialog(this);
		eventID = (TextView) findViewById(R.id.txtShow_eventID);
		creator = (TextView) findViewById(R.id.txtShow_eventCreator);
		title = (TextView) findViewById(R.id.txtShow_eventTitle);
		description = (TextView) findViewById(R.id.txtShow_Desc);
		time = (TextView) findViewById(R.id.txtShow_eventTime);
		listOfJoiner = (ListView) findViewById(R.id.listOfJoiner);
		btn_startEvent = (Button)findViewById(R.id.btn_JoinShare);
		btn_startEvent.setBackgroundResource(R.drawable.startevent_btn);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*-----------Full Screen ------------*/
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/*-----------------------------------*/
		setContentView(R.layout.join_show);
		initial();
		
		(new AsyncTask<Void, Void, String>(){
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				startProgressDialog("loading");
			}
			@Override
			protected String doInBackground(Void... params) {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/event");
				HttpResponse response;
				InputStream stream = null;
				
				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					nameValuePairs.add(new BasicNameValuePair("eventID", getIntent().getExtras().getString("eventID")));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					
					response = httpclient.execute(httppost);
					
					stream = response.getEntity().getContent();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
		        BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
		        String jsonString = "";
		        String line;
		        try {
					while ((line = reader.readLine()) != null) {
						jsonString += line;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
		        
				return jsonString;
			}
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if(result.compareTo("EMPTY") == 0){
					Toast.makeText(getBaseContext(), "Event not found", 1).show();
					finish();
				} else {
//					Toast.makeText(getBaseContext(), result, 1).show();
//					startActivityForResult(new Intent(Act_Join_Main.this, Act_Join_ShowDetail.class).putExtra("eventID", event_id).putExtra("eventJson", result), 1);
					setDetail(result);
					
					share = getIntent().getBooleanExtra("share", false);
					
					if(share){
						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("text/plain");
						intent.putExtra(Intent.EXTRA_TEXT, "http://whenwhereshare.appspot.com/event?eventID="+getIntent().getStringExtra("eventID"));
						startActivity(Intent.createChooser(intent, "Share with"));
					}
				}
				stopProgressDialog();
			}
    	}).execute();
	}
	
	private ProgressDialog progress;
	private boolean dialogIsWorking = false;
	private void startProgressDialog(String message){
		progress.setCancelable(false);
		progress.setMessage(message);
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}
	
	private void setDetail(String jsonString){
		JSONObject json = null;
		try {
			json = Util.parseJson(jsonString);
			eventID.setText(getIntent().getStringExtra("eventID"));
			creator.setText(json.getString("creator"));
			title.setText(json.getString("title"));
			description.setText(json.getString("description"));
			time.setText(json.getString("date") + " " + json.getString("time"));
		} catch (JSONException e) {
			e.printStackTrace();
			Toast.makeText(getBaseContext(), jsonString, 1).show();
		} catch (FacebookError e) {
			e.printStackTrace();
		}
		
		/*joiner list*/
		try {
			int numJoiner = json.getJSONArray("joiner").length();
			joinerID = new String[numJoiner];
			joiner = new String[numJoiner];
			for(int i=0;i<numJoiner;i++){
				joinerID[i] = json.getJSONArray("joiner").getJSONObject(i).getString("uid");
				joiner[i] = json.getJSONArray("joiner").getJSONObject(i).getString("name");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		listOfJoiner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, joiner));
		listOfJoiner.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.facebook.com/"+joinerID[arg2])));
			}
		});
		
		/*start event button*/
		btn_startEvent.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivityForResult(new Intent(Act_StartEvent.this, Act_ChoosePlace.class).putExtra("eventID", getIntent().getStringExtra("eventID")), 1);
			}
		});
	}
	
	private MenuItem menu_share;
	private MenuItem menu_delEvent;
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		menu_share = menu.add("Share Event");
		menu_share.setIcon(android.R.drawable.ic_menu_share);
		menu_delEvent = menu.add("Delete Event");
		menu_delEvent.setIcon(android.R.drawable.ic_menu_delete);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item == menu_share){
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, "http://whenwhereshare.appspot.com/event?eventID="+getIntent().getStringExtra("eventID"));
			startActivity(Intent.createChooser(intent, "Share with"));
		}
		
		if(item == menu_delEvent){
			(new AsyncTask<Void, Void, Void>(){
				@Override
				protected void onPreExecute() {
					super.onPreExecute();
					startProgressDialog("deleting");
				}
				@Override
				protected Void doInBackground(Void... params) {
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/del");
					
					try {
						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
						nameValuePairs.add(new BasicNameValuePair("eventID", getIntent().getExtras().getString("eventID")));
						httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						
						httpclient.execute(httppost);
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}
				@Override
				protected void onPostExecute(Void result) {
					super.onPostExecute(result);
					stopProgressDialog();
					startActivity(new Intent(Act_StartEvent.this, Act_Main.class));
					finish();
				}
			}).execute();
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == 1 && resultCode == RESULT_OK){
			startActivity(new Intent(this, Act_Done.class).putExtra("eventID", getIntent().getExtras().getString("eventID")));
			finish();
		}
	}
	
}
