package org.cpe22.wws;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class Act_Create_When extends Activity {
	private Button goWhere;
	private EditText title;
	private EditText desc;
	private TimePicker timepick;
	private DatePicker datepick;
	private Event event;
	private void initialize(){
//		event = getIntent().getParcelableExtra("event");
		event = new Event();
		goWhere = (Button)findViewById(R.id.btn_Where);
		title = (EditText)findViewById(R.id.titleEvent);
		timepick = (TimePicker)findViewById(R.id.timeEvent);
		datepick  = (DatePicker)findViewById(R.id.dateEvent);
		desc = (EditText)findViewById(R.id.descEvent);
	}
	private String checkTime(int in){
		String out= "";
		if(in < 10)
			out = "0"+String.valueOf(in);
		else
			out = String.valueOf(in);
		return out;
	}
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
        setContentView(R.layout.create_when);
        initialize();
        goWhere.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(title.getText().length() > 0){
	            	Log.d("TITLE", title.getText().toString());
	                /*Perform action on click*/
	            	event.setTitle(title.getText().toString());
	            		int date = datepick.getDayOfMonth();
	            		int month = datepick.getMonth();
	            		int year = datepick.getYear();
	            	event.setDate(checkTime(date)+"/"+checkTime(month)+"/"+checkTime(year));
	            		int hour = timepick.getCurrentHour();
	            		int min = timepick.getCurrentMinute();
	            	event.setTime(checkTime(hour) + ":" + checkTime(min));
	            	if(desc.getText().toString().length() > 0)
	            		event.setDesc(desc.getText().toString());
	            	else
	            		event.setDesc("No description given");
	            	Intent intent = new Intent(getApplicationContext(), Act_Create_Where.class);
	            	intent.putExtra("event", event);
	            	startActivityForResult(intent, 1);
            	}
            	else
            		Toast.makeText(getApplicationContext(), "Title cannot be empty", Toast.LENGTH_SHORT).show();
            }
        });
    }
    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == 1 && resultCode == RESULT_OK){
			setResult(RESULT_OK, data);
			finish();
		}
	}
    
}
