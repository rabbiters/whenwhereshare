package org.cpe22.wws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.android.Facebook;

public class Act_Main extends Activity {
	
	private SharedPreferences sharePrefer;
	public static SharedPreferences.Editor editor;
	public static String token;
	public static String uid;
	public static String name;
	public static boolean ongoing = false;
	public static boolean connected = true;
	private boolean share = false;
//	private Event event;
	/* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
        setContentView(R.layout.main);
        initialize();
        
        sharePrefer = getSharedPreferences("access_token", MODE_PRIVATE);
        editor = sharePrefer.edit();
        token = sharePrefer.getString("token", null);
        uid = sharePrefer.getString("uid", null);
        name = sharePrefer.getString("name", null);
        if(token == null){
        	startActivity(new Intent(this, Act_Login.class));
        	finish();
        	return;
        } else {
//        	event.setCreator(uid);
//        	event.setName(name);
        	
        }
        
        /* ---------- Call GPS module guideline ---------- */
        //gps();
    }
	
    /*initialize*/
	private Button createBtn;
	private Button joinBtn;
	private void initialize(){
//		event = new Event();
		progress = new ProgressDialog(this);
		createBtn = (Button)findViewById(R.id.btnCreate);
		joinBtn  = (Button)findViewById(R.id.btnJoin);
		methodToCreate();
	}
	
	private void methodToCreate(){
		createBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*Perform action on click*/
            	Intent intent = new Intent(getApplicationContext(), Act_Create_When.class);
//            	intent.putExtra("event", event);
            	startActivityForResult(intent, 1);
            }
        });
		joinBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /*Perform action on click*/
            	Intent intent = new Intent(getApplicationContext(), Act_Join_Main.class);
            	startActivityForResult(intent, 2);
            }
        });
	}
	
	private MenuItem menu_logout;
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu_logout = menu.add("Logout");
		menu_logout.setIcon(android.R.drawable.ic_menu_revert);
		return true;
	}

	private ProgressDialog pd;
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item == menu_logout){
			(new AsyncTask<Void, Void, Void>() {
				@Override
				protected void onPreExecute() {
					super.onPreExecute();
					pd = new ProgressDialog(Act_Main.this);
					pd.setCancelable(false);
					pd.setMessage("logging out");
					pd.show();
				}
				@Override
				protected Void doInBackground(Void... params) {
					Act_Main.editor.clear();
					Act_Main.editor.commit();
					Facebook facebook = new Facebook("249069241853063");
					try {
						facebook.logout(Act_Main.this);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}
				@Override
				protected void onPostExecute(Void result) {
					super.onPostExecute(result);
					pd.dismiss();
					startActivity(new Intent(Act_Main.this, Act_Login.class));
					finish();
				}
			}).execute();
		}
		return false;
	}
	
	private ProgressDialog progress;
	private boolean dialogIsWorking = false;
	private void startProgressDialog(){
		progress.setCancelable(false);
		progress.setMessage("checking");
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
//		if(requestCode == 1 && resultCode == RESULT_OK){
//			Intent intent = new Intent(Intent.ACTION_SEND);
//			intent.setType("text/plain");
//			intent.putExtra(Intent.EXTRA_TEXT, data.getStringExtra("link"));
//			startActivity(Intent.createChooser(intent, "Share with"));
//		}
//		
//		if(requestCode == 2 && resultCode == RESULT_OK){
//			Intent intent = new Intent(Intent.ACTION_SEND);
//			intent.setType("text/plain");
//			intent.putExtra(Intent.EXTRA_TEXT, "http://whenwhereshare.appspot.com/event?eventID="+data.getStringExtra("eventID"));
//			startActivity(Intent.createChooser(intent, "Share with"));
//		}
		share = true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		/*check on going event*/
    	(new AsyncTask<Void, Void, String>(){
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				startProgressDialog();
			}
			@Override
			protected String doInBackground(Void... params) {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/status?uid="+uid);
				HttpResponse response;
				InputStream stream = null;
				
				try {
					response = httpclient.execute(httppost);
					
					stream = response.getEntity().getContent();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
					connected = false;
				} catch (IOException e) {
					e.printStackTrace();
					connected = false;
				}
				
				if(!connected)
					return null;
				
		        BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
		        String jsonString = "";
		        String line;
		        try {
					while ((line = reader.readLine()) != null) {
						jsonString += line;
					}
				} catch (IOException e) {
					e.printStackTrace();
					connected = false;
				}
		        
				return jsonString;
			}
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				stopProgressDialog();
				if(connected){
					if(result.compareTo("0") == 0){
//						Toast.makeText(getBaseContext(), "no on going", 1).show();
//					} else if(result.compareTo("1") == 0){
//						Toast.makeText(getBaseContext(), "on going", 1).show();
//						startActivity(new Intent(Act_Main.this, Act_StartEvent.class).putExtra("eventID", result));
//						finish();
					} else {
//						Toast.makeText(getBaseContext(), "on going", 1).show();
						if(result.charAt(0) == 'z')
							startActivity(new Intent(Act_Main.this, Act_StartEvent.class).putExtra("eventID", result.substring(1)).putExtra("share", share));
						else if(result.charAt(0) == 'x')
							startActivity(new Intent(Act_Main.this, Act_Done.class).putExtra("eventID", result.substring(1)));
						else
							startActivity(new Intent(Act_Main.this, Act_Unjoin.class).putExtra("eventID", result).putExtra("share", share));
						finish();
					}
				} else {
					Toast.makeText(getBaseContext(), "ERROR : Internet Connection", 1).show();
					finish();
				}
			}
    	}).execute();
	}

	/* ---------- this below code is guideline for GPS module ---------- */
//	LocationManager locationManag;
//	Location location;
//	private void gps(){
//		/* Get location manager */
//		locationManag = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//		
//		/* Check GPS setting; GPS satellite, Cell phone tower/Network Wifi */
//		checkEnableGPS();
//		
//		/* Get location from Cell phone tower/Network Wifi base */
//		location = locationManag.getLastKnownLocation(android.location.LocationManager.NETWORK_PROVIDER);
//		
//		/* Get location from GPS satellite */
////		location = locationManag.getLastKnownLocation(android.location.LocationManager.GPS_PROVIDER);
//		
//		startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:"+String.valueOf(location.getLatitude())+","+String.valueOf(location.getLongitude())+"z=1")));
//		
//		/* Show current location */
//		Toast.makeText(getBaseContext(), String.valueOf(location.getLatitude())+" , "+String.valueOf(location.getLongitude()), 1).show();
//	}
//	
//	private void checkEnableGPS(){
//		/* Go to GPS setting if it is not set */
//		if(!locationManag.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER)){
//			startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//		}
//	}
	
	/* LocationListener, I cannot see it is necessary now, but it up to you if you want to play with them */
//	private LocationListener locationListen = new LocationListener() {
//		@Override
//		public void onStatusChanged(String provider, int status, Bundle extras) {
//			
//		}
//		@Override
//		public void onProviderEnabled(String provider) {
//			
//		}
//		@Override
//		public void onProviderDisabled(String provider) {
//			
//		}
//		@Override
//		public void onLocationChanged(Location location) {
//			
//		}
//	};
}
