package org.cpe22.wws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class Act_ChoosePlace extends Activity {

	private String eventID = null;
	private int numJoiner;
	private double lat = 0;
	private double lon = 0;
	
	private Place place[];
	private String namePlaces[];
	private int indexPlace = -1;
	
	private ListView listPlace;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
		setContentView(R.layout.chooseplace);
		
		eventID = getIntent().getStringExtra("eventID");
		progress = new ProgressDialog(this);
		listPlace = (ListView)findViewById(R.id.listPlace);
		
		(new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				startProgressDialog("calculating");
			}
			@Override
			protected Void doInBackground(Void... params) {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/cal");
				HttpResponse response;
				InputStream stream = null;
				
				try {
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					nameValuePairs.add(new BasicNameValuePair("eventID", eventID));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					
					response = httpclient.execute(httppost);
					
					stream = response.getEntity().getContent();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
		        BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
		        String jsonString = "";
		        String line;
		        try {
					while ((line = reader.readLine()) != null) {
						jsonString += line;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
		        
		        try {
					JSONObject json = Util.parseJson(jsonString);
					numJoiner = json.getJSONArray("joiner").length();
					for(int i=0;i<numJoiner;i++){
						lat += Double.valueOf(json.getJSONArray("joiner").getJSONObject(i).getString("lat"));
						lon += Double.valueOf(json.getJSONArray("joiner").getJSONObject(i).getString("lon"));
					}
					lat = lat / numJoiner;
					lon = lon / numJoiner;
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (FacebookError e) {
					e.printStackTrace();
				}
		        
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				stopProgressDialog();
				Toast.makeText(getBaseContext(), String.valueOf(lat)+" , "+String.valueOf(lon), 1).show();
				fsq.execute();
			}
		}).execute();
	}
	
	private AsyncTask<Void, Void, String> fsq = new AsyncTask<Void, Void, String>(){
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			startProgressDialog("loading places");
		}
		@Override
		protected String doInBackground(Void... params) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String curentDateandTime = sdf.format(new Date());
			String url = "https://api.foursquare.com/v2/venues/explore?client_id=LDJ1MDIG0KKMCODZYP1GUYIHHFE1WLINNIJ4VOR0NMMFRIHX&client_secret=IT0CCIUADFEVOYLUU4JWC410JBDRYRAKT5STXQFACVZQMCGC&v="+curentDateandTime;
			url += "&ll=" + lat + "," + lon;
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httppost = new HttpGet(url);
			HttpResponse response;
			InputStream stream = null;
			
			try {
				response = httpclient.execute(httppost);
				stream = response.getEntity().getContent();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
	        BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
	        String jsonString = "";
	        String line;
	        try {
				while ((line = reader.readLine()) != null) {
					jsonString += line;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	        
	        int numPlaces = 0;
	        JSONObject json = null;
	        JSONArray places = null;
	        
			try {
				json = Util.parseJson(jsonString);
				places = json.getJSONObject("response").getJSONArray("groups").getJSONObject(0).getJSONArray("items");
			} catch (JSONException e1) {
				e1.printStackTrace();
			} catch (FacebookError e1) {
				e1.printStackTrace();
			}
				
			numPlaces = places.length();
			place = new Place[numPlaces];
			namePlaces = new String[numPlaces];
			
			for(int i=0;i<numPlaces;i++){
				place[i] = new Place();
				try {
					place[i].name = places.getJSONObject(i).getJSONObject("venue").getString("name");
				} catch (JSONException e) {
					e.printStackTrace();
					place[i].name = "";
				}
				try {
					place[i].lat = places.getJSONObject(i).getJSONObject("venue").getJSONObject("location").getDouble("lat");
				} catch (JSONException e) {
					e.printStackTrace();
					place[i].lat = 0;
				}
				try {
					place[i].lon = places.getJSONObject(i).getJSONObject("venue").getJSONObject("location").getDouble("lng");
				} catch (JSONException e) {
					e.printStackTrace();
					place[i].lon = 0;
				}
				try {
					place[i].address = places.getJSONObject(i).getJSONObject("venue").getJSONObject("location").getString("address");
				} catch (JSONException e) {
					e.printStackTrace();
					place[i].address = "";
				}
				try {
					place[i].city = places.getJSONObject(i).getJSONObject("venue").getJSONObject("location").getString("city");
				} catch (JSONException e) {
					e.printStackTrace();
					place[i].city = "";
				}
				try {
					place[i].postalCode = places.getJSONObject(i).getJSONObject("venue").getJSONObject("location").getString("postalCode");
				} catch (JSONException e) {
					e.printStackTrace();
					place[i].postalCode = "";
				}
				namePlaces[i] = place[i].name;
			}
//					
//			try {
//				return places.getJSONObject(0).getJSONObject("venue").getJSONObject("location").toString();
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
			return null;
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			stopProgressDialog();
//			Toast.makeText(getBaseContext(), result, 1).show();
			listPlace.setAdapter(new ArrayAdapter<String>(Act_ChoosePlace.this, android.R.layout.simple_list_item_1, namePlaces));
			listPlace.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					indexPlace = arg2;
					AlertDialog.Builder confirmDialog = new AlertDialog.Builder(Act_ChoosePlace.this);
					confirmDialog.setIcon(android.R.drawable.ic_menu_mapmode);
					confirmDialog.setTitle("Confirmation");
					confirmDialog.setMessage("Do you want to choose '" + namePlaces[arg2] + "' as meeting place?");
					confirmDialog.setPositiveButton("Yes", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							/*----------choose place----------*/
							placeTask.execute();
						}
					});
					confirmDialog.setNegativeButton("No", null);
					confirmDialog.show();
				}
			});
		}
	};
	
	private ProgressDialog progress;
	private boolean dialogIsWorking = false;
	private void startProgressDialog(String message){
		progress.setCancelable(false);
		progress.setMessage(message);
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}

	private AsyncTask<Void, Void, Void> placeTask = new AsyncTask<Void, Void, Void>(){
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			startProgressDialog("Saving choosen place");
		}
		@Override
		protected Void doInBackground(Void... params) {
			place();
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			stopProgressDialog();
			setResult(RESULT_OK);
			finish();
		}
	};
	
	private void place(){
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/place");
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
		nameValuePairs.add(new BasicNameValuePair("eventID", eventID));
		nameValuePairs.add(new BasicNameValuePair("namePlace", place[indexPlace].name));
		nameValuePairs.add(new BasicNameValuePair("lat", String.valueOf(place[indexPlace].lat)));
		nameValuePairs.add(new BasicNameValuePair("lon", String.valueOf(place[indexPlace].lon)));
		String address = "";
		if(place[indexPlace].address.length() > 0)
			address += place[indexPlace].address;
		if(place[indexPlace].city.length() > 0){
			if(address.length() > 0)
				address += ", ";
			address += place[indexPlace].city;
		}
		if(place[indexPlace].postalCode.length() > 0){
			if(address.length() > 0)
				address += ", ";
			address += place[indexPlace].postalCode;
		}
		nameValuePairs.add(new BasicNameValuePair("address", address));
		
		try {
//			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			httpclient.execute(httppost);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
