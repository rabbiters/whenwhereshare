package org.cpe22.wws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.android.FacebookError;
import com.facebook.android.Util;
public class Act_Done extends Activity {
	private String eventID = null;
	private String namePlace = null;
	private String address = "";
	private double lat, lon; 
	private TextView txtEventID;
	private TextView txtEventPlace;
	private TextView txtEventAddr;
	private Button btnShowMap;
	private Button btnDone;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*-----------Full Screen ------------*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*-----------------------------------*/
        setContentView(R.layout.done);
		progress = new ProgressDialog(this);
		eventID = getIntent().getStringExtra("eventID");
		txtEventID = (TextView) findViewById(R.id.txt_eventID);
		txtEventPlace = (TextView) findViewById(R.id.txt_eventPlace);
		txtEventAddr  = (TextView) findViewById(R.id.txt_eventAddress);
		btnShowMap = (Button) findViewById(R.id.btn_showMap);
		btnDone = (Button) findViewById(R.id.btn_done);
		(new AsyncTask<Void, Void, String>(){
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				startProgressDialog("Loading event details..");
			}
			@Override
			protected String doInBackground(Void... params) {
				HttpClient httpclient = new DefaultHttpClient();
				HttpGet httppost = new HttpGet("http://whenwhereshare.appspot.com/done?eventID="+eventID);
				HttpResponse response;
				InputStream stream = null;
				
				try {
					response = httpclient.execute(httppost);
					stream = response.getEntity().getContent();
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
		        BufferedReader reader = null;
				try {
					reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
		        String jsonString = "";
		        String line;
		        try {
					while ((line = reader.readLine()) != null) {
						jsonString += line;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
		        
		        JSONObject json = null;
		        try {
					json = Util.parseJson(jsonString);
					
					namePlace = json.getString("namePlace");
					address = json.getString("address");
					lat = json.getDouble("lat");
					lon = json.getDouble("lon");
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (FacebookError e) {
					e.printStackTrace();
				}
		        
				return jsonString;
			}
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				stopProgressDialog();
				//Toast.makeText(getBaseContext(), namePlace+" "+address+" "+String.valueOf(lat)+" "+String.valueOf(lon), 1).show();
				txtEventID.setText(eventID);
				txtEventPlace.setText(namePlace);
				if(address.length()== 0 || address == null)
					txtEventAddr.setText("No address provided");
				else
					txtEventAddr.setText(address);
				btnShowMap.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						String uri = "geo:"+lat+","+lon+"?z=20";
						intent.setData(Uri.parse(uri)); // set the Uri
						startActivity(intent);
					}
				});
				btnDone.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						doneTask.execute();
					}
				});
			}
		}).execute();
	}
	private ProgressDialog progress;
	private boolean dialogIsWorking = false;
	private void startProgressDialog(String message){
		progress.setCancelable(false);
		progress.setMessage(message);
		progress.show();
		dialogIsWorking = true;
	}
	private void stopProgressDialog(){
		if(dialogIsWorking){
			progress.dismiss();
			dialogIsWorking = false;
		}
	}
	private AsyncTask<Void, Void, Void> doneTask = new AsyncTask<Void, Void, Void>(){
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			startProgressDialog("Completing");
		}
		@Override
		protected Void doInBackground(Void... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://whenwhereshare.appspot.com/done");
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("eventID", eventID));
			nameValuePairs.add(new BasicNameValuePair("uid", Act_Main.uid));
			
			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
				httpclient.execute(httppost);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			stopProgressDialog();
			startActivity(new Intent(Act_Done.this, Act_Main.class));
			finish();
		}
	};
}
